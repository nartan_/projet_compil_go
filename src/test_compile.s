	.text
	.globl	main
main:
	call F_main
	xorq %rax, %rax
	ret
F_main:
	movq %rsp, %rbp
	movq $1, %rdi
	pushq %rdi
	movq $2, %rdi
	popq %rbx
	cmpq %rdi, %rbx
	setg %r10b
	movzbq %r10b, %rdi
	xorq %rbx, %rbx
	cmpq %rdi, %rbx
	jne L_2
	movq $2, %rdi
	pushq %rdi
	movq $2, %rdi
	popq %rbx
	cmpq %rdi, %rbx
	sete %r10b
	movzbq %r10b, %rdi
	xorq %rbx, %rbx
	cmpq %rdi, %rbx
	je L_3
	movq $1, %rdi
	pushq %rdi
	movq $1, %rdi
	popq %rbx
	cmpq %rdi, %rbx
	setne %r10b
	movzbq %r10b, %rdi
	xorq %rbx, %rbx
	cmpq %rdi, %rbx
	jne L_4
	movq $1, %rdi
L_4:
L_3:
L_2:
	call print_bool
ret
print_int:
        movq    %rdi, %rsi
        movq    $S_int, %rdi
        xorq    %rax, %rax
        call    printf
        ret

print_string:
        movq %rdi, %rsi
        movq $S_str, %rdi
        xorq %rax, %rax
        call printf
        ret

print_addr:
        movq %rdi, %rsi
        movq $S_ptr, %rdi
        xorq %rax, %rax
        call printf
        ret

print_bool:
        movq %rdi, %rsi
        xorq %rax, %rax
        cmpq %rdi, %rax
        jz L_false
        movq $S_true, %rdi
        call printf
        ret
L_false:
        movq $S_false, %rdi
        call printf
        ret
	.data
S_int:
	.string "%ld"
S_str:
	.string "%s"
S_ptr:
	.string "%p"
S_true:
	.string "true"
S_false:
	.string "false"

(* étiquettes
     F_function      entrée fonction
     E_function      sortie fonction
     L_xxx           sauts
     S_xxx           chaîne

   expression calculée avec la pile si besoin, résultat final dans %rdi

   fonction : arguments sur la pile, résultat dans %rax ou sur la pile

            res k
            ...
            res 1
            arg n
            ...
            arg 1
            adr. retour
   rbp ---> ancien rbp
            ...
            var locales
            ...
            calculs
   rsp ---> ...

*)

open Format
open Ast
open Tast
open X86_64
open Typing

let debug = ref false

let strings = Hashtbl.create 32
let alloc_string =
  let r = ref 0 in
  fun s ->
    incr r;
    let l = "S_" ^ string_of_int !r in
    Hashtbl.add strings l s;
    l

let malloc n = movq (imm n) (reg rdi) ++ call "malloc"
let allocz n = movq (imm n) (reg rdi) ++ call "allocz"

let sizeof = Typing.sizeof

let new_label =
  let r = ref 0 in fun () -> incr r; "L_" ^ string_of_int !r


let curr_ofs = ref 0


type env = {
  exit_label: string;
  ofs_this: int;
  nb_locals: int ref; (* maximum *)
  next_local: int; (* 0, 1, ... *)
}

let empty_env =
  { exit_label = ""; ofs_this = -1; nb_locals = ref 0; next_local = 0 }

let mk_bool d = { expr_desc = d; expr_typ = Tbool }

(* f reçoit le label correspondant à ``renvoyer vrai'' *)
let compile_bool f =
  let l_true = new_label () and l_end = new_label () in
  f l_true ++
  movq (imm 0) (reg rdi) ++ jmp l_end ++
  label l_true ++ movq (imm 1) (reg rdi) ++ label l_end

let tvoid = Tmany []

let rec expr env e = match e.expr_desc with
  | TEskip ->
    nop
  | TEconstant (Cbool true) ->
    movq (imm 1) (reg rdi)
  | TEconstant (Cbool false) ->
    movq (imm 0) (reg rdi)
  | TEconstant (Cint x) ->
    movq (imm64 x) (reg rdi)
  | TEnil ->
    xorq (reg rdi) (reg rdi)
  | TEconstant (Cstring s) ->
    (* TODO_ code pour constante string *)
    let string_label = alloc_string s in
    movq (ilab string_label) (reg rdi)
  | TEbinop (Band, e1, e2) ->
    (* TODO_ code pour ET logique lazy *)
    let instant_false = new_label () in
    (expr env e1) ++ (xorq (reg rbx) (reg rbx)) ++ (cmpq (reg rdi) (reg rbx)) ++ (je instant_false) ++ (expr env e2) ++ (label instant_false)
  | TEbinop (Bor, e1, e2) ->
    (* TODO_ code pour OU logique lazy *)
    let instant_true = new_label () in
    (expr env e1) ++ (xorq (reg rbx) (reg rbx)) ++ (cmpq (reg rdi) (reg rbx)) ++ (jne instant_true) ++ (expr env e2) ++ (label instant_true)
  | TEbinop (Blt | Ble | Bgt | Bge as op, e1, e2) ->
    (* TODO_ code pour comparaison ints *)
    begin
    match op with
        |Blt -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (cmpq (reg rdi) (reg rbx)) ++ (setl (reg r10b)) ++ (movzbq (reg r10b) rdi)
                (*negative returns true, otherwise, return false*) 
        |Ble -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (cmpq (reg rdi) (reg rbx)) ++ (setle (reg r10b)) ++ (movzbq (reg r10b) rdi)
        |Bgt -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (cmpq (reg rdi) (reg rbx)) ++ (setg (reg r10b)) ++ (movzbq (reg r10b) rdi)
        |Bge -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (cmpq (reg rdi) (reg rbx)) ++ (setge (reg r10b)) ++ (movzbq (reg r10b) rdi)
        |_ -> failwith "compiler error : invalid pattern matching, expected a binop for integers (comparison)"
    end 
  | TEbinop (Badd | Bsub | Bmul | Bdiv | Bmod as op, e1, e2) ->
    (* TODO_ code pour arithmetique ints *) (*assert false*)
    begin
    match op with
        |Badd -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (addq (reg rbx) (reg rdi)) 
        |Bsub -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (subq (reg rdi) (reg rbx)) ++ (movq (reg rbx) (reg rdi))
        |Bmul -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (imulq (reg rbx) (reg rdi))
        |Bdiv -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rax) ++ (xorq (reg rdx) (reg rdx)) ++ (idivq (reg rdi)) ++ (movq (reg rax) (reg rdi))
        |Bmod -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rax) ++ (xorq (reg rdx) (reg rdx)) ++ (idivq (reg rdi)) ++ (movq (reg rdx) (reg rdi))
        |_ -> failwith "compiler error : invalid pattern matching, expected a binop for integers (arithmetic)"
    end 
  | TEbinop (Beq | Bne as op, e1, e2) ->
    (* TODO_ code pour egalite toute valeur *) (* TODO si on cherche à comparer deux strings qui ne sont pas stockées au même endroit cela ne fonctionnera pas*)
    begin
    match op with
        | Beq -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (cmpq (reg rdi) (reg rbx)) ++ (sete (reg r10b)) ++ (movzbq (reg r10b) rdi)
        | Bne -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env e2) ++ (popq rbx) ++ (cmpq (reg rdi) (reg rbx)) ++ (setne (reg r10b)) ++ (movzbq (reg r10b) rdi)
        | _ -> failwith "compiler error : expected a comparator equal or not equal"
    end 
  | TEunop (Uneg, e1) ->
    (* TODO_ code pour negation ints *)
    (expr env e1) ++ (negq (reg rdi)) 
  | TEunop (Unot, e1) ->
    (* TODO_ code pour negation bool *)
    (expr env e1) ++ (xorb (reg r10b) (reg r10b)) ++ (movzbq (reg r10b) rdx) ++ (cmpq (reg rdi) (reg rdx)) ++ (sete (reg r10b)) ++ (movzbq (reg r10b) rdi) 
  | TEunop (Uamp, e1) ->
    (* TODO_ code pour & *)
    let typ = e1.expr_typ in
    (expr env e1) ++ (movq (reg rdi) (reg rdx)) ++ (malloc (sizeof typ)) ++ (movq (reg rdx) (ind rdi))
  | TEunop (Ustar, e1) ->
    (* TODO_ code pour * *)
    (expr env e1) ++ (movq (ind rdi) (reg rdi)) 
  | TEprint el ->
    (* TODO code pour Print *) (*assert false*)
    begin
    match el with
      |[] -> nop
      |h::t -> let exp_t = {expr_desc=TEprint t; expr_typ=tvoid} in
               begin
               match h.expr_typ with
                 | Tint -> (expr env h) ++ (call "print_int") ++ (expr env exp_t)
                 | Tstring -> (expr env h) ++ (call "print_string") ++ (expr env exp_t) (* TODO_ réparer pour appeller avec avec %s à la place*)
                 | Tptr _ | Tstruct _ -> (expr env h) ++ (call "print_addr") ++ (expr env exp_t)
                 | Tbool -> (expr env h) ++ (call "print_bool") ++ (expr env exp_t)
                 | Tmany l -> assert false (* TODO implement print for Tmany*)
               end
    end 
  | TEident x ->
    (* TODO_ code pour x *)
    (movq (reg rbp) (reg rdi)) ++ (movq (imm x.v_ofs) (reg rdx)) ++ (addq (reg rdx) (reg rdi)) ++ (movq (ind rdi) (reg rdi))
  | TEassign ([{expr_desc=TEident x}], [e1]) ->
    (* TODO_ code pour x := e *)
    let assign_var x e =
           (expr env e) ++ (movq (imm x.v_ofs) (reg rdx)) ++ (addq (reg rbp) (reg rdx)) ++ (movq (reg rdi) (ind rdx))
    in
    assign_var x e1 
  | TEassign ([lv], [e1]) ->
    (* TODO_ code pour x1,... := e1,... *)
    begin
    match lv.expr_desc with
      | TEunop (Ustar, lvreg) -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env lvreg) ++ (popq rdx) ++ (movq (reg rdx) (ind rdi))
      | _ -> (expr env e1) ++ (pushq (reg rdi)) ++ (expr env lv) ++ (popq rdx) ++ (movq (reg rdx) (ind rdi))
    end
  | TEassign (lvl, el) -> (* TODO_ cas avec plusieurs éléments*)
    begin
    match lvl, el with
      | hlv::tlv, elh::elt ->
        let h_assign = {expr_desc=(TEassign ([hlv], [elh])); expr_typ=tvoid} in
        let t_assign = {expr_desc=(TEassign (tlv, elt)); expr_typ=tvoid} in
        (expr env h_assign) ++ (expr env t_assign)
      | _ -> failwith "compiler error, number of left values and expressions do not match"
    end
  | TEblock el ->
     (* TODO_ code pour block *)(*assert false*)
     let decl_var curr_env var =
             let new_ofs = curr_env.ofs_this - 8 in
             var.v_ofs <- new_ofs;
             curr_env.nb_locals := !(curr_env.nb_locals) + 1;
             let new_next_local = curr_env.next_local + 1 in
             let new_env = {exit_label=curr_env.exit_label; ofs_this=new_ofs; nb_locals=curr_env.nb_locals; next_local=new_next_local} in
             new_env
     in

     begin
     match el with
       |[] -> nop
       |h::t -> let exp_t = {expr_desc=TEblock t; expr_typ=tvoid} in
                begin
                match h.expr_desc with
                | TEvars x -> let new_env =  List.fold_left decl_var env x in
                              (pushq (imm 0)) ++ (expr new_env exp_t) ++ (popq rdx)
                | _ ->  (expr env h) ++ (expr env exp_t)
                end
     end

  | TEif (e1, e2, e3) ->
     (* TODO_ code pour if *)
     let end_if_label = new_label () in
     let else_branch_label = new_label () in
     (expr env e1) ++ (xorq (reg rdx) (reg rdx)) ++ (cmpq (reg rdi) (reg rdx)) ++ (jz else_branch_label) ++ (expr env e2) ++ (jmp end_if_label) ++ (label else_branch_label) ++ (expr env e3) ++ (label end_if_label)
  | TEfor (e1, e2) ->
     (* TODO_ code pour for *)
     let for_loop_label = new_label () in
     let exit_loop_label = new_label () in
     (label for_loop_label) ++ (expr env e1) ++ (xorq (reg rdx) (reg rdx)) ++ (cmpq (reg rdi) (reg rdx)) ++ (jz exit_loop_label) ++ (expr env e2) ++ (jnz for_loop_label) ++ (label exit_loop_label)
  | TEnew ty ->
     (* TODO_ code pour new S *)
     let size = sizeof ty in
     (malloc size)
  | TEcall (f, el) ->
     (* TODO code pour appel fonction *) assert false
  | TEdot (e1, {f_ofs=ofs}) ->
     (* TODO code pour e.f *) assert false
  | TEvars x -> (* TODO *)
     (*assert false*) (* fait dans block *) assert false
  | TEreturn [] ->
    (* TODO code pour return e *) assert false
  | TEreturn [e1] ->
    (* TODO code pour return e1,... *) assert false
  | TEreturn _ ->
     assert false
  | TEincdec (e1, op) ->
    (* TODO code pour return e++, e-- *) (*assert false*)
    (* e1 is supposed to be a variable, we should have its adress, we then need to add 1 to whatever number is stored there and then return the original value*)
    (* for variables we have the option of using the variable ids as the addresses directly (not particularly clean) of to keep track of where in the memory the id would be relative to an original adress*)
    match op with
      | Inc -> assert false
      | Dec -> assert false

let function_ f e =
  if !debug then eprintf "function %s:@." f.fn_name;
  let empty_env = {exit_label=new_label (); ofs_this = 0; nb_locals = ref 0; next_local = 0} in
  (* TODO code pour fonction *) let s = f.fn_name in ((label ("F_" ^ s)) ++ (movq (reg rsp) (reg rbp)) ++  (expr empty_env e) ++ inline "ret") (*solution temporaire*)

let decl code = function
  | TDfunction (f, e) -> code ++ function_ f e
  | TDstruct _ -> code

let file ?debug:(b=false) dl =
  debug := b;
  (* TODO calcul offset champs *)
  (* TODO code fonctions *) let funs = List.fold_left decl nop dl in
  { text =
      globl "main" ++ label "main" ++
      call "F_main" ++
      xorq (reg rax) (reg rax) ++
      ret ++
      funs ++
      inline "
print_int:
        movq    %rdi, %rsi
        movq    $S_int, %rdi
        xorq    %rax, %rax
        call    printf
        ret
" ++
      inline "
print_string:
        movq %rdi, %rsi
        movq $S_str, %rdi
        xorq %rax, %rax
        call printf
        ret
" ++
      inline "
print_addr:
        movq %rdi, %rsi
        movq $S_ptr, %rdi
        xorq %rax, %rax
        call printf
        ret
" ++
      inline "
print_bool:
        movq %rdi, %rsi
        xorq %rax, %rax
        cmpq %rdi, %rax
        jz L_false
        movq $S_true, %rdi
        call printf
        ret
L_false:
        movq $S_false, %rdi
        call printf
        ret
"
; (* TODO print pour d'autres valeurs *)
   (* TODO appel malloc de stdlib *)
    data =
      label "S_int" ++ string "%ld" ++
      label "S_str" ++ string "%s" ++
      label "S_ptr" ++ string "%p" ++
      label "S_true" ++ string "true" ++
      label "S_false" ++ string "false" ++
      (Hashtbl.fold (fun l s d -> label l ++ string s ++ d) strings nop)
    ;
  }


open Format
open Lib
open Ast
open Tast

let debug = ref false

let dummy_loc = Lexing.dummy_pos, Lexing.dummy_pos

exception Error of Ast.location * string

let error loc e = raise (Error (loc, e))


let found_main = ref false

(* variable globale pour id des variables *)
let var_id_num = ref 0

(* TODO_ environnement pour les types structure *)
let context_struct = ref [] (*only contains structure names*)
let context_struct_all = ref [] (*contains the full structures*)


(* fonctions utiles *)

(* associe à un nom de structure valide la structure complète associée *)
let find_struct struct_id =
  let rec explore = function
    | [] -> failwith ("ce : Undefined structure : " ^ struct_id) (* ne devrait jamais arriver *)
    | h::t when h.s_name = struct_id -> h
    | h::t -> explore t
  in
  explore !context_struct_all


(* conversion de ptyp à typ *)
let rec ptyp_to_typ = function
  |PTident ident when ident.id = "bool" -> Tbool
  |PTident ident when ident.id = "int" -> Tint
  |PTident ident when ident.id = "string" -> Tstring
  |PTident ident when List.mem ident.id !context_struct -> Tstruct (find_struct ident.id)
  |PTptr ptyp -> Tptr (ptyp_to_typ ptyp)
  |PTident ident -> raise (Error (ident.loc, "undefined structure"))




let verif_in_list structure l =
  if List.mem structure.id !l  then raise (Error (structure.loc, "structure déjà définie"))
  else context_struct := structure.id ::(!l)


let rec print_context_struct = function
  |[] -> ()
  |h::t -> (print_string(h); print_string("\n"); print_context_struct(t);)


let rec verif_types_struct = function
  |[] -> ()
  |h::t -> match snd h with
             |PTident ident  when (List.mem ident.id !context_struct) || (List.mem ident.id ["int"; "string"; "bool"]) -> verif_types_struct t
             |PTident ident -> raise (Error (ident.loc, ("undefined type : " ^ ident.id)))
             |PTptr ptr -> verif_types_struct ((fst h, ptr)::t)
  

 let rec verif_struct_dupl used_field_names = function
   |[] -> ()
   |h::t when List.mem (fst h).id used_field_names -> raise (Error ((fst h).loc, "champ déjà défini"))
   |h::t -> verif_struct_dupl ((fst h).id::used_field_names) t
 


(* TODO_ environnement pour les fonctions *)
let context_fun = ref []

(* fonctions utiles *)

(* prend en entrée le nom d'une fonction et donne la structure fonction associée à celui-ci*)
let find_fun fun_name =
  let rec explore = function
    |[] -> failwith ("function : " ^ fun_name ^ " does not exist")
    |h::t when h.pf_name.id = fun_name -> h
    |_::t -> explore t
  in
  explore !context_fun

(* convertit une pfonction en fonction_*)
let pfun_to_fun f =
  let rec param_to_var = function
    |[] -> []
    |h::t -> let var_name = (fst h).id in
             let var_id = !var_id_num + 1 in
             var_id_num := var_id;
             let var_loc = (fst h).loc in
             let var_typ = ptyp_to_typ (snd h) in
             let var = {v_name=var_name; v_id=var_id; v_loc=var_loc; v_typ=var_typ; v_used=false; v_addr=false; v_ofs=(-1)} in
             var::(param_to_var t)
  in
  let var_list = param_to_var f.pf_params in
  let typ_list = List.map ptyp_to_typ f.pf_typ in
  {fn_name=f.pf_name.id; fn_params=var_list; fn_typ=typ_list}

let verif_fun f l =
  
  let rec verif_name name = function
    |[] -> context_fun := f::(!context_fun)
    |h::t when name = h.pf_name.id -> raise (Error (h.pf_name.loc, "fonction déjà définie"))
    |h::t -> verif_name name t
  in

  let rec verif_param_name used_param_names params  = match params with
    |[] -> ()
    |h::t when List.mem (fst h).id used_param_names -> raise (Error ((fst h).loc, "paramètre déjà utilisé"))
    |h::t -> verif_param_name (((fst h).id)::used_param_names) t
  in

  let rec verif_types = function
    |[] -> ()
    |h::t -> match h with
               |PTident h_ident when List.mem h_ident.id ((!context_struct)@["int"; "bool"; "string"]) -> verif_types t
               |PTident h_ident -> raise (Error (h_ident.loc, ("undefined type : " ^ h_ident.id)))
               |PTptr ptr -> verif_types (ptr::t)
  in
  (if f.pf_name.id = "main" then 
   begin
     found_main := true;
     (if f.pf_params != [] then raise (Error (dummy_loc, "function main takes no arguments")));
     (if f.pf_typ != [] then raise (Error (dummy_loc, "function main has invalid return type")));
   end);
  verif_name f.pf_name.id !context_fun; verif_param_name [] f.pf_params; verif_types f.pf_typ

let rec print_context_fun = function
  |[] -> ()
  |h::t -> print_string "\n"; print_context_fun t



let rec type_type = function
  | PTident { id = "int" } -> Tint
  | PTident { id = "bool" } -> Tbool
  | PTident { id = "string" } -> Tstring
  | PTptr ty -> Tptr (type_type ty)
  | _ -> error dummy_loc ("unknown struct ") (* TODO type structure *)

let rec eq_type ty1 ty2 = match ty1, ty2 with
  | Tint, Tint | Tbool, Tbool | Tstring, Tstring -> true
  | Tstruct s1, Tstruct s2 -> s1 == s2
  | Tptr ty1, Tptr ty2 -> eq_type ty1 ty2
  | _ -> false
    (* TODO autres types *)

let fmt_used = ref false
let fmt_imported = ref false

let evar v = { expr_desc = TEident v; expr_typ = v.v_typ }

let new_var =
  let id = ref 0 in
  fun x loc ?(used=false) ty ->
    incr id;
    { v_name = x; v_id = !id; v_loc = loc; v_typ = ty; v_used = used; v_addr = false; v_ofs=(-1) }

module Env = struct
  module M = Map.Make(String)
  type t = var M.t
  let empty = M.empty
  let find = M.find
  let add env v = M.add v.v_name v env

  let all_vars = ref []
  let check_unused () =
    let check v =
      if v.v_name <> "_" && not v.v_used (* TODO_ used *) then error v.v_loc "unused variable" in
    List.iter check !all_vars


  let var x loc ?used ty env =
    let v = new_var x loc ?used ty in
    all_vars := v :: !all_vars;
    add env v, v

  (* TODO type () et vecteur de types *)
end

let tvoid = Tmany []
let make d ty = { expr_desc = d; expr_typ = ty } (* TODO use function to replace all the times where this is done manually*)
let stmt d = make d tvoid




let rec expr env e =
 let e, ty, rt = expr_desc env e.pexpr_loc e.pexpr_desc in
  { expr_desc = e; expr_typ = ty }, rt

and typ_to_str = function
      |Tint -> "int"
      |Tbool -> "bool"
      |Tstring -> "string"
      |Tmany (h::t) -> ";" ^ (typ_to_str h) ^ (typ_to_str (Tmany t)) ^ " "
      |Tmany [] -> " Tmany"
      |Tptr typ -> "*" ^ (typ_to_str typ)
      |Tstruct structure ->  structure.s_name

(* checks if two types are equal (otherwise structures with different fields will be counted as different)*)
and typ_eq typ_1 typ_2 = match typ_1, typ_2 with
      |Tint, Tint -> true
      |Tbool, Tbool -> true
      |Tstring, Tstring -> true
      |Tptr r_1, Tptr r_2 -> typ_eq r_1 r_2
      |Tmany l1, Tmany l2 -> let rec iter_eq = function
                               |[], [] -> true
                               |h1::t1, h2::t2 -> (typ_eq h1 h2) && (iter_eq (l1, l2))
                               |_ -> false
                             in iter_eq (l1, l2)
      |Tstruct struct1, Tstruct struct2 -> struct1.s_name = struct2.s_name
      |_ -> false


(* fonction pour convertir les listes  pexpr en exp*)
and  exp_list env = function
    |[] -> []
    |h::t -> let h_exp_desc, h_typ, h_val = expr_desc env h.pexpr_loc h.pexpr_desc in
             let h_exp = {expr_desc=h_exp_desc; expr_typ=h_typ} in
             h_exp::(exp_list env t)
 
(* vérifie que la pexpr d'entrée est bien une l_value*)
and is_l_val = function
  |PEunop (Ustar, exp) when exp.pexpr_desc != PEnil -> true
  |PEident _ -> true
  |PEdot (pexp, field) -> is_l_val pexp.pexpr_desc
  |_ -> false


and exp_l_to_typ_l l = 
     let rec l_exp_to_typ_l is_list = function
       |[] -> []
       |[{expr_desc=(TEcall (func, _))}] when not is_list -> func.fn_typ
       |h_exp::t_exp -> h_exp.expr_typ :: (l_exp_to_typ_l true t_exp)
     in
     l_exp_to_typ_l false l

(* TODO vérifier expressions Tmany avec appels et tout*)
and expr_desc env loc = function
  
  | PEskip ->
     TEskip, tvoid, false
  | PEconstant c ->
    (* TODO_ *) (*TEconstant c, tvoid, false*)
    begin
    match c with
      |Cbool _ -> TEconstant c, Tbool, false
      |Cint _ -> TEconstant c, Tint, false
      |Cstring _ -> TEconstant c, Tstring, false
    end

  | PEbinop (op, e1, e2) ->
    (* TODO_ *) (*assert false*)
    let e1exp_desc, e1typ, e1val = expr_desc env e1.pexpr_loc e1.pexpr_desc in
    let e2exp_desc, e2typ, e2val = expr_desc env e2.pexpr_loc e2.pexpr_desc in
    let e1exp = {expr_desc=e1exp_desc; expr_typ=e1typ} in
    let e2exp = {expr_desc=e2exp_desc; expr_typ=e2typ} in
    begin
    match op with 
      |Badd |Bsub |Bmul |Bdiv |Bmod ->
        begin
        match e1typ, e2typ with
          |Tint, Tint -> (TEbinop (op, e1exp, e2exp), Tint, false)
          |Tint, _ -> raise (Error (e2.pexpr_loc, "this expression has type : " ^ (typ_to_str e2typ) ^ ", an expression of type int was expected"))
          |_, _ -> raise (Error (e1.pexpr_loc, ("this expression has type : " ^ (typ_to_str e1typ) ^ ", an expression of type int was expected")))
        end
    |Blt |Ble |Bgt |Bge -> (* TODO Beq et Bne doivent pouvoir comparer touts les types de données*)
        begin
        match e1typ, e2typ with
          |Tint, Tint -> (TEbinop (op, e1exp, e2exp), Tbool, false)
          |Tint, _ -> raise (Error (e2.pexpr_loc, "this expression has type : " ^ (typ_to_str e2typ) ^ ", an expression of type int was expected"))
          |_, _ -> raise (Error (e1.pexpr_loc, "this expression has type : " ^ (typ_to_str e1typ) ^ ", an expression of type int was expected"))
        end
    |Band |Bor ->
        begin
        match e1typ, e2typ with
          |Tbool, Tbool -> (TEbinop (op, e1exp, e2exp), Tbool, false)
          |Tbool, _ -> raise (Error (e2.pexpr_loc, "this expression has type : " ^ (typ_to_str e2typ) ^ ", an expression of type bool was expected"))
          |_, _ -> raise (Error (e1.pexpr_loc, "this expression has type : " ^ (typ_to_str e1typ) ^ ", an expression of type bool was expected"))
        end
     | Beq | Bne ->
        begin
        match e1typ, e2typ with
          |typ1, typ2 when typ1 = tvoid && typ2 = tvoid -> raise (Error (e2.pexpr_loc, "cannot compare two void expressions"))
          |typ1, typ2 when typ1 = typ2 -> (TEbinop (op, e1exp, e2exp), Tbool, false)
          |typ1, typ2 -> raise (Error (e2.pexpr_loc, "this expression has type : " ^ (typ_to_str e2typ) ^ ", but an expression of type : " ^ (typ_to_str e1typ) ^ " was expected"))
        end
  end
   
  | PEunop (Uamp, e1) ->
    (* TODO_ *) (*assert false*)
    (if not (is_l_val e1.pexpr_desc) then raise (Error (e1.pexpr_loc, "this expression expected a left value")));
    let e1_exp_desc, e1_typ, _ = expr_desc env e1.pexpr_loc e1.pexpr_desc in
    let e1_exp = {expr_desc=e1_exp_desc; expr_typ=e1_typ} in
    (TEunop (Uamp, e1_exp)), Tptr e1_typ, false

  | PEunop (Uneg | Unot | Ustar as op, e1) ->
    (* TODO_ *) (*assert false*)
    let e1_exp_desc, e1_typ, e1_val = expr_desc env e1.pexpr_loc e1.pexpr_desc in
    let e1_exp = {expr_desc=e1_exp_desc; expr_typ=e1_typ} in
    begin
    match op with
      |Uneg when e1_typ = Tint -> (TEunop (op, e1_exp), Tint, false)
      |Unot when e1_typ = Tbool -> (TEunop(op, e1_exp), Tbool, false)
      |Ustar -> (*assert false*) (* TODO_ *)
                begin
                match e1_typ with
                  |Tptr typ when typ=tvoid -> raise (Error (loc, "this expression has type nil, an pointer was expected"))
                  |Tptr typ -> (TEunop (Ustar, e1_exp)), typ, false
                  |_ -> raise (Error (e1.pexpr_loc, "this expression has the wrong type a pointer was expected"))
                end
      | _ -> raise (Error (loc, "this expression has the wrong type")) 
    end
    

  | PEcall ({id = "fmt.Print"}, el) ->
    (* TODO_ *) (*TEprint [], tvoid, false*)
    fmt_used := true;
    let t_exp_list = exp_list env el in (TEprint t_exp_list), tvoid, false
               
  | PEcall ({id="new"}, [{pexpr_desc=PEident {id}}]) ->
     let ty = match id with
       |"int" -> Tint | "bool" -> Tbool | "string" -> Tstring
       |type_id when List.mem type_id !context_struct  -> Tstruct (find_struct type_id)
       |_ -> (* TODO_ *) error loc ("no such type " ^ id)
     in (* TODO_ : confirmer que c'est bon pour cette partie*)
     TEnew ty, Tptr ty, false
  | PEcall ({id="new"}, _) ->
     error loc "new expects a type"
  | PEcall (id, el) ->
     (* TODO_ *) (*assert false*)
     let func = find_fun id.id in
     let rec new_in_l = function
       |[] -> []
       |h::t -> let h_exp_desc, h_typ, _ = expr_desc env h.pexpr_loc h.pexpr_desc in
                let h_exp = {expr_desc=h_exp_desc; expr_typ=h_typ} in
                h_exp::(new_in_l t)
     in
     let in_exp_l = new_in_l el in
     let in_typ_l = exp_l_to_typ_l in_exp_l in
     let f_param_typ f_param = ptyp_to_typ (snd f_param) in
     let rec verif_args in_params_typ f_params = match in_params_typ, f_params with
       | [], [] -> ()
       | h_in_typ::t, h_f_param::t' when h_in_typ = (f_param_typ h_f_param) -> verif_args t t'
       | h_in_typ::t, h_f_param::t' when h_in_typ != (f_param_typ h_f_param) -> raise (Error (loc, ("parameter has type : " ^ (typ_to_str h_in_typ) ^ " a parameter of type : " ^ (typ_to_str (f_param_typ  h_f_param)) ^ " was expected"))) (*loc is wrong but not worth hassle*)
       |_ -> raise (Error (loc, "the number of parameters given does not match that of the function"))
     in
     verif_args in_typ_l func.pf_params;
     let new_fun = pfun_to_fun func in
     begin
     match new_fun.fn_typ with
       | [fun_typ] -> ((TEcall (new_fun, in_exp_l)), fun_typ, false)
       | typ_l -> ((TEcall (new_fun, in_exp_l)), (Tmany typ_l), false)
     end
  | PEfor (e, b) ->
     (* TODO_ *) (*assert false*)
     let e_exp_desc, e_typ, e_val = expr_desc env e.pexpr_loc e.pexpr_desc in
     let b_exp_desc, b_typ, b_val = expr_desc env b.pexpr_loc b.pexpr_desc in
     let e_exp = {expr_desc=e_exp_desc; expr_typ=e_typ} in
     let b_exp = {expr_desc=b_exp_desc; expr_typ=b_typ} in
     begin
     match e_typ with
       |Tbool -> (TEfor (e_exp, b_exp)), b_typ, false
       |_ -> raise (Error (e.pexpr_loc, "this expression has the wrong type, an expression of type bool was expected"))
     end

  | PEif (e1, e2, e3) ->
     (* TODO_ *) (*assert false*)
     let e1_exp_desc, e1_typ, e1_val = expr_desc env e1.pexpr_loc e1.pexpr_desc in
     let e2_exp_desc, e2_typ, e2_val = expr_desc env e2.pexpr_loc e2.pexpr_desc in
     let e3_exp_desc, e3_typ, e3_val = expr_desc env e3.pexpr_loc e3.pexpr_desc in
     let e1_exp = {expr_desc=e1_exp_desc; expr_typ=e1_typ} in
     let e2_exp = {expr_desc=e2_exp_desc; expr_typ=e2_typ} in
     let e3_exp = {expr_desc=e3_exp_desc; expr_typ=e3_typ} in
     begin
     match e1_typ with
       |Tbool -> (TEif (e1_exp, e2_exp, e3_exp), Tmany [e2_typ; e3_typ], false) (* PFUCK *) (*grosse incertitude sur le type renvoyé*)
       |_ -> raise (Error (e1.pexpr_loc, "this expression has the wrong type, an expression of type bool was expected"))
     end
  | PEnil ->
     (* TODO_ *) (*assert false*)
     TEnil, Tptr tvoid, false
  | PEident {id=id} ->
     (* TODO_ *) (try let v = Env.find id env in
                     v.v_used <- true;
                     TEident v, v.v_typ, false
     with Not_found -> error loc ("unbound variable " ^ id))
  | PEdot (e, id) ->
     (* TODO_ *) (*assert false*)
     let e_exp_desc, e_typ, _ = expr_desc env e.pexpr_loc e.pexpr_desc in
     let e_exp = {expr_desc=e_exp_desc; expr_typ=e_typ} in
     begin
     match e_typ with
       | Tstruct structure | Tptr (Tstruct structure) -> let fields = structure.s_fields in
                                                         let field_of_s = Hashtbl.find_opt fields id.id in
                                                         begin
                                                         match field_of_s with
                                                           | None -> raise (Error (e.pexpr_loc, "this structure contains no such field"))
                                                           | Some field -> (TEdot (e_exp, field)), field.f_typ, false
                                                         end
       | _ -> raise (Error (e.pexpr_loc, "this structure does not have any fields"))
     end
  | PEassign (lvl, el) ->
     (* TODO_ *) (*TEassign ([], []), tvoid, false*)
     begin
     match lvl, el with
       |[], [] -> TEassign ([], []), tvoid, false
       |h_lv::t_lvl, e::t_el -> let h_lv_exp_desc, h_lv_typ, _ = expr_desc env h_lv.pexpr_loc h_lv.pexpr_desc in
                                let e_exp_desc, e_typ, _ = expr_desc env e.pexpr_loc e.pexpr_desc in
                                let h_lv_exp = {expr_desc=h_lv_exp_desc; expr_typ=h_lv_typ} in (* TODO_ ajouter utilisation des variables ?*)
                                let e_exp = {expr_desc=e_exp_desc; expr_typ=e_typ} in
                                (if not (is_l_val h_lv.pexpr_desc) then raise (Error (loc, "expected a left value")));
                                (if not (typ_eq h_lv_typ e_typ) then raise (Error (loc, ("tried to assign type : "^(typ_to_str e_typ)^" to a variable of type : "^(typ_to_str h_lv_typ)))));
                                let t_exp_desc, _, _ = expr_desc env loc (PEassign (t_lvl, t_el)) in
                                begin
                                match t_exp_desc with
                                  |TEassign (lvl_exp, el_exp) -> (TEassign (h_lv_exp::lvl_exp, e_exp::el_exp)), tvoid, false
                                  |_ -> raise (Error (loc, "ce : this expression has the wrong type expected an expression of type TEassign"))
                                end
       |_, _ -> raise (Error (loc, "the number of variables does not match the number of expressions"))
     end

  | PEreturn el ->
     (* TODO_ *) (*TEreturn [], tvoid, true*)
     let exp_l = exp_list env el in
     ((TEreturn exp_l), tvoid, true)
  | PEblock el -> (* TODO_ ajouter copie d'environements *)
     (* TODO_ *) (*TEblock [], tvoid, false*)
     begin
     match el with
       |[] -> TEblock [], tvoid, false
       |h::t -> begin
                let h_exp_desc, h_typ, h_ret = expr_desc env h.pexpr_loc h.pexpr_desc in
                let rec add_vars_env envt = function
                  |[] -> envt
                  |h_var::t -> add_vars_env (Env.add envt h_var) t
                in
                let updated_env envt exp_desc = match exp_desc with
                  | TEvars var_l -> add_vars_env envt var_l
                  | TEblock ({expr_desc=(TEvars var_l); _}::t) when h_typ = Tmany [tvoid] -> add_vars_env envt var_l
                  |_ -> envt
                in
                let new_env = updated_env env h_exp_desc in
                let t_exp_desc, t_typ, t_ret = expr_desc new_env loc (PEblock t) in (*unsure about the location (second argument)*)
                (if h_ret && t_ret then raise (Error (loc, "this code contains unreachable parts")));
                let h_exp = {expr_desc=h_exp_desc; expr_typ=h_typ} in
                match t_exp_desc with
                  |TEblock tl -> let new_exp = match h_exp_desc, h_typ with
                                                 | TEblock l, Tmany [tvoid] -> TEblock (l@tl)
                                                 | _ -> TEblock (h_exp :: tl)
                                 in
                                 (new_exp, tvoid, h_ret || t_ret)
                  |_ -> raise (Error (loc, "ce this expression has the wrong type, an expression of type block was expected"))
                end
     end
  | PEincdec (e, op) ->
     (* TODO_ *) (*assert false*) (* ajouter vérification que c'est bien une Lvalue*)
     let e_exp_desc, e_typ, e_val = expr_desc env e.pexpr_loc e.pexpr_desc in
     let e_exp = {expr_desc=e_exp_desc; expr_typ=e_typ} in
     if not (is_l_val e.pexpr_desc) then raise (Error (e.pexpr_loc, "this expression is not assignable"));
     begin
     match e_typ with (* TODO_ ajouter vérification que e est bien une l_value*)
       |Tint -> (TEincdec (e_exp, op)), tvoid, false (* TODO_ vérifier que l'opération renvoie bien un entier (pas trouvé dans la sémantique)*)
       |_ -> raise (Error (e.pexpr_loc, "this expression has the wrong type, an expression of type int was expected"))
    end
  | PEvars (id_l, ptyp_opt, pexpr_l) ->
    (* TODO_ *) (*assert false*)
    let rec extract_exp = function
      |[] -> []
      |h::t -> let h_exp_desc, h_typ, _ = expr_desc env h.pexpr_loc h.pexpr_desc in
               let h_exp = {expr_desc=h_exp_desc; expr_typ=h_typ} in
               h_exp :: (extract_exp t)
    in
    let rec create_vars = function
      |[], [] -> []
      |h_ident::t, h_typ::t' -> var_id_num := !var_id_num + 1;
                              let h_var = {v_name=h_ident.id; v_id=(!var_id_num); v_loc=h_ident.loc; v_typ=h_typ; v_used=false; v_addr=false; v_ofs=(-1)} in
                              let var_l = create_vars (t, t') in
                              Env.all_vars := h_var::(!Env.all_vars);
                              h_var::var_l
      |_, _ -> raise (Error (loc, "the number of variables and types do not match"))
    in
    let rec ident_exp_l = function
      | [] -> []
      | h::t -> {expr_desc=TEident h; expr_typ=h.v_typ} :: (ident_exp_l t)
    in
    begin
    match ptyp_opt with
      |None -> let exp_l = extract_exp pexpr_l in
               let typ_l = exp_l_to_typ_l exp_l in
               let var_l = create_vars (id_l, typ_l) in
               let var_exp = {expr_desc=TEvars var_l; expr_typ=Tmany typ_l} in
               let ident_exp_l = ident_exp_l var_l in
               let assign_exp = {expr_desc=TEassign (ident_exp_l, exp_l); expr_typ = tvoid} in
               TEblock (var_exp::[assign_exp]), Tmany [tvoid], false
      |Some ptyp -> let var_l = create_vars (id_l, (List.init (List.length id_l) (fun _ -> (ptyp_to_typ ptyp)))) in
                    (TEvars var_l), ptyp_to_typ ptyp, false
     end
   




(* 1. declare structures *)
let phase1 = function
        | PDstruct { ps_name = { id = id; loc = loc }; ps_fields=fl} -> (* TODO_ *)
                                                    let res:ident = {id = id; loc = loc} in
                                                    verif_in_list res context_struct;
                                                    let pre_fields = Hashtbl.create 5 in
                                                    let pre_struct = {s_name=id; s_fields=pre_fields} in
                                                    context_struct_all := pre_struct :: (!context_struct_all)
                                                    
  | PDfunction _ -> ()

let rec sizeof = function
  | Tint | Tbool | Tstring | Tptr _ -> 8
  | Tstruct structure ->
        let total_size = ref 0 in
        let add_size name field = total_size := !total_size + (sizeof field.f_typ) in
        Hashtbl.iter add_size structure.s_fields;
        !total_size
  | _ -> (* TODO *) assert false 




(* 2. declare functions and type fields *)
let phase2 = function
  | PDfunction  f -> (* TODO_ *)
                     verif_fun f context_fun
                              
  | PDstruct { ps_name = {id}; ps_fields = fl} -> (* TODO_ *)
                                                   verif_types_struct fl;
                                                   verif_struct_dupl [] fl;
                                                   let rec add_fields structure = function
                                                     |[] -> ()
                                                     |h_pfield::t -> let field_ident, field_ptyp = h_pfield in
                                                                     let field_typ = ptyp_to_typ field_ptyp in
                                                                     let new_field = {f_name=field_ident.id; f_typ=field_typ; f_ofs=0} in
                                                                     Hashtbl.add structure.s_fields field_ident.id new_field;
                                                                     add_fields structure t
                                                   in
                                                   let rec complete_struct struct_id = function
                                                     |[] -> failwith "ce : structure not found"
                                                     |h::t when h.s_name = struct_id -> add_fields h fl
                                                     |_::t -> complete_struct struct_id t
                                                   in
                                                   complete_struct id !context_struct_all
                                                   (*
                                                   let rec ps_to_s_fields = function
                                                      |[] -> Hashtbl.create 3
                                                      |h::t -> let hashtbl = ps_to_s_fields t in
                                                               let new_typ = ptyp_to_typ (snd h) in
                                                               let new_field = {f_name=(fst h).id; f_typ=new_typ; f_ofs=0} in (*aucune idée de ce que fait f_ofs*)
                                                               Hashtbl.add hashtbl (fst h).id new_field; hashtbl
                                                    in
                                                    context_struct_all := {s_name=id; s_fields=(ps_to_s_fields fl)} :: (!context_struct_all)*)
                                                   

(* 3. type check function bodies *)
let decl = function
        | PDfunction { pf_name={id; loc}; pf_body = e; pf_typ=tyl; pf_params=param_l } ->
    (* TODO_ check name and type *) 
    let f = { fn_name = id; fn_params = []; fn_typ = []} in
    let empty_env = Env.empty in
    let rec param_env envt = function
      |[] -> envt
      |h_param::t_param -> let param_id, param_ptyp = h_param in
                           let param_typ = ptyp_to_typ param_ptyp in
                           var_id_num := !var_id_num + 1;
                           let new_var = {v_name=param_id.id; v_loc=param_id.loc; v_id=(!var_id_num); v_typ=param_typ; v_used=false; v_addr=false; v_ofs=(-1)} in
                           let new_env = Env.add envt new_var in
                           param_env new_env t_param
    in
    let new_env = param_env empty_env param_l in
    let e, rt = expr new_env e in
    TDfunction (f, e)
  | PDstruct {ps_name={id}} ->
    (* TODO *) let s = { s_name = id; s_fields = Hashtbl.create 5 } in
     TDstruct s

let file ~debug:b (imp, dl) =
  debug := b;
  (* fmt_imported := imp; *)
  List.iter phase1 dl;
  List.iter phase2 dl;
  (*print_context_struct !context_struct;
  print_context_fun !context_fun;*)
  if not !found_main then error dummy_loc "missing method main";
  let dl = List.map decl dl in
  Env.check_unused (); (* TODO_ variables non utilisees *)
  if imp && not !fmt_used then error dummy_loc "fmt imported but not used";
  if not imp && !fmt_used then error dummy_loc "fmt package not imported";
  dl
